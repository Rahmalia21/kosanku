<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_admin= new User();
        $user_admin->name = 'Admin Master';
        $user_admin->email= 'adminmaster@gmail.com';
        $user_admin->password = bcrypt('Adminmaster1234');
        $user_admin->save();
    }
}
