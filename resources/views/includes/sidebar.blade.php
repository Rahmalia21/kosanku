<aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.html"><i class
                            ="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <li class="menu-title">Data Kots</li><!-- /.menu-title -->
                    <li class="">
                        <a href="/datakos"> <i class="menu-icon fa fa-list"></i>Lihat Data</a>
                    </li>
                    <li class="">
                        <a href="#"> <i class="menu-icon fa fa-plus"></i>Tambah Data</a>
                    </li>

                    <li class="menu-title">Data Admin</li><!-- /.menu-title -->
                    <li class="">
                        <a href="/dataadmin"> <i class="menu-icon fa fa-list"></i>Lihat Data Admin</a>
                    </li>
                    <li class="">
                        <a href="#"> <i class="menu-icon fa fa-plus"></i>Tambah Data Amin</a>
                    </li>

                    <!-- <li class="menu-title">Transaksi</li>
                    <li class="">
                        <a href="#"> <i class="menu-icon fa fa-list"></i>Lihat Transaksi</a>
                    </li>
 -->                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>